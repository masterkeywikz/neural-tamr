# all the constants 
ROOT_FORM = '_ROOT_FORM_'
ROOT_LEMMA = '_ROOT_LEMMA_'
ROOT_POS = '_ROOT_POS_'
ROOT_CPOS = '_ROOT_CPOS_'

EMPTY = None

OTHER=u'<O>'
PRED=u'<pred-%s>'
NONPRED=u'<nonpred>'
NEGATION=u'<negation>'
AMRUNK=u'<amrunk>'
NUM=u'<number>'
ROLE=u'<role>' # have-x-role
PLACEHOLDER='<x>'