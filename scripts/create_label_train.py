'''
create label with threshold for training
'''
from __future__ import print_function
import sys

def extract(label_codebook_path, train_label_path):
    label_codebook = dict((w.strip(),i) for i,w in enumerate(open(label_codebook_path)))
    new_train_label_path = train_label_path+'.theta30'
    with open(train_label_path) as tf, open(new_train_label_path,'w') as ntf:
        for line in tf:
            line = line.strip()
            new_labels_per_line = [w if w in label_codebook else '<DICT>' for w in line.split()]
            ntf.write(' '.join(new_labels_per_line))
            ntf.write('\n')

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('Usage: python %s [label_codebook_path] [train_label_path]' % __file__)
    extract(sys.argv[1],sys.argv[2])