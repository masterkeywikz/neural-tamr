"""
extract tokenized sentences from semeval amr file
"""

from __future__ import absolute_import
from __future__ import print_function

import sys, os
import codecs
import re
from codecs import open

CURDIR=os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(CURDIR))

from preprocessing import load_xml_instances

def extract_feat(input_xml):
    instances = load_xml_instances(input_xml)
    basename = input_xml.rsplit('.',3)[0]

    output_lemma = basename + '.lemma'
    output_pos = basename + '.pos'
    output_ner = basename + '.ner'
    output_pfx = basename + '.pfx'
    output_sfx = basename + '.sfx'
    with open(output_lemma,'w',encoding='utf8') as lf, open(output_pos,'w',encoding='utf8') as pf,open(output_ner,'w',encoding='utf8') as of,open(output_pfx,'w',encoding='utf8') as pff,open(output_sfx,'w',encoding='utf8') as sff:
        for inst in instances:
            lemmas = []
            poss = []
            ners = []
            prefixes = []
            suffixes = []
            for tok in inst.tokens[1:]:
                word = tok['form']
                lemma = tok['lemma']
                pos = tok['pos']
                ner = tok['ne']
                prefix = word[:2]
                suffix = word[-2:]

                lemmas.append(lemma)
                poss.append(pos)
                ners.append(ner)
                prefixes.append(prefix)
                suffixes.append(suffix)
            lf.write(' '.join(lemmas)+'\n')
            pf.write(' '.join(poss)+'\n')
            of.write(' '.join(ners)+'\n')
            pff.write(' '.join(prefixes)+'\n')
            sff.write(' '.join(suffixes)+'\n')
            

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('Usage: python extract_feat.py [input_xml_file]')
        exit(1)

    extract_feat(sys.argv[1])
