
DATA_PATH=$1
DATADIR="$( cd "$( dirname "${1}" )" && pwd )"
DIR="$( cd "$( dirname "${BASH_SOURCE[ 0]}" )" && pwd )"
CORENLP_PATH="/home/j/llc/cwang24/Tools/stanford-corenlp-full-2015-04-20"

$(which python) $DIR/extract_tok.py $1

java -Xmx25000m -cp "${CORENLP_PATH}/*" edu.stanford.nlp.pipeline.StanfordCoreNLP -props "${CORENLP_PATH}/default.properties" -file $DATA_PATH.sent.tok -outputDirectory $DATADIR

mv $DATA_PATH.sent.tok.xml $DATA_PATH.sent.tok.prp.xml
