cp ../../AMRParsing/data/semeval/xml-data/dev.txt.sent.tok ../../AMRParsing/data/semeval/xml-data/dev.txt.tok
cp ../../AMRParsing/data/semeval/xml-data/training.txt.sent.tok ../../AMRParsing/data/semeval/xml-data/training.txt.tok
python2.7 ../scripts/extract_feat.py ../../AMRParsing/data/semeval/xml-data/dev.txt.sent.prp.xml
python2.7 ../scripts/extract_feat.py ../../AMRParsing/data/semeval/xml-data/training.txt.sent.prp.xml
THEANO_FLAGS=device=gpu,floatX=float32 python3.5 bilstm.py --mode test --test_dir ../../AMRParsing/data/semeval/xml-data/ --test_fn training.txt --save_model ../models/model.word.ner.pfx.sfx.h5 2>test.training.cm.word.ner.pfx.sfx.log
THEANO_FLAGS=device=gpu,floatX=float32 python3.5 bilstm.py --mode test --test_dir ../../AMRParsing/data/semeval/xml-data/ --test_fn dev.txt --save_model ../models/model.word.ner.pfx.sfx.h5 2>test.dev.cm.word.ner.pfx.sfx.log
