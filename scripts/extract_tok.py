"""
extract tokenized sentences from semeval amr file
"""

from __future__ import absolute_import
from __future__ import print_function

import sys, os
import codecs
import re

CURDIR=os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(CURDIR))

from preprocessing import readAMR

def clean_sent(sent):
    '''
    cleaning the sentence: remove emoji, hashtag stuff
    however this will lead to tokenization mismatch
    '''
    junk_pattern = re.compile('@-@ ')
    return junk_pattern.sub('',sent)
    
    

def extract_tok_sent(input_amr_file):
    comments, amrs = readAMR(input_amr_file)
    tok_sent_file = input_amr_file+'.sent.tok'
    with codecs.open(tok_sent_file,'w',encoding='utf8') as tokwf:
        sents = [c['tok'] for c in comments]
        tokwf.write('\n'.join(sents))


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('Usage: python extract_tok.py [input_amr_file]')
        exit(1)

    extract_tok_sent(sys.argv[1])
