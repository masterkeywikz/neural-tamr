'''
categorize each input toke into predefined
concept tag using alignment, without
considering the edge
'''

from __future__ import print_function
from __future__ import division
from __future__ import absolute_import

from preprocessing import preprocess
import sys,re, codecs
from pprint import pprint
from collections import defaultdict
from constants import OTHER, PRED, NONPRED, NEGATION, AMRUNK, NUM, ROLE, PLACEHOLDER#, CORENE
from common.util import Quantity, Interrogative, Polarity, StrLiteral, EdgeType
import cPickle as pkl

import logger
logger.file = codecs.open(__file__+'.log','w',encoding='utf8')
import gflags
FLAGS=gflags.FLAGS
gflags.DEFINE_boolean('FLAG_VERB',False,'flag to enable using of verbalization list')

def extract_quantity(curr_amr, path):

    end = len(path) - 3 # parent
    assert end > -1
    curr = path[end]
    cpt = curr_amr.node_to_concepts.get(curr, curr)
    cpt_comp = cpt.split('~')
    if len(cpt_comp) == 1 and (cpt.endswith('-quantity') or cpt.endswith('-entity')):
        return cpt
    else:
        return NUM
    # while end >= 0:
    #     if len(cpt.split('~')) == 2: # find first not aligned to sentence
    #         break
    #     curr = path[end]
    #     if isinstance(curr, EdgeType):
    #         end -= 1
    #         continue
    #     cpt = curr_amr.node_to_concepts.get(curr, curr)
    #     frag_elements.append(cpt.split('~')[0])

    #     end -= 1
    #return frag_elements
                
def extract_name_entity(curr_amr, path):

    end = len(path) - 5 # parent
    #assert end > -1, path
    if end < 0:
        end = len(path) - 3
        logger.writeln('WARNING: No type found for named entity %s' % path)
    curr = path[end]
    cpt = curr_amr.node_to_concepts.get(curr,curr)
    cpt_comp = cpt.split('~')
    #assert len(cpt_comp) == 1, cpt_comp
    return cpt_comp[0]
    
    # while end >= 0:
    #     curr = path[end]
    #     if isinstance(curr, EdgeType):
    #         end -= 1
    #         continue
    #     cpt = curr_amr.node_to_concepts.get(curr, curr)
    #     if cpt == 'name': # name concept not aligned
    #         end -= 1
    #         continue
    #     frag_elements.append(cpt.split('~')[0])
    #     if len(cpt.split('~')) == 1: # find first not aligned to sentence
    #         break
    #     end -= 1
    return frag_elements

def categorize_concept(input_amr, output_label):

    instances = preprocess(input_amr)
    candidate_tagset = defaultdict(int)
    label_wf = open(output_label, 'w')
    
    for inst in instances:
        _, s2c_alignment = inst.alignment
        curr_amr = inst.amr
        for i, tok in enumerate(inst.tokens):
            if i > 0: # first is dummy token
                concept_tag = 'None'
                curr_frag = s2c_alignment[i-1]
                
                if not curr_frag: # not aligned 
                    concept_tag = OTHER
                else:
                    if len(curr_frag) == 1:
                        concept_tag = get_concept_tag(curr_amr, curr_frag[0])
                    else:
                        cpt_labels = []
                        cpt_type_set = set([])
                        unique_frag = []
                        for frag in sorted(curr_frag, key=lambda x:len(x)):
                            frag_type, incoming, path = curr_amr.get_concept_relation(frag)
                            cpt_type_set.add(frag_type)
                            var = path[-1]
                            cpt_label = curr_amr.node_to_concepts.get(var, var)
                            cpt_label, align = cpt_label.split('~')
                            if cpt_label not in cpt_labels:
                                cpt_labels.append(cpt_label)
                                unique_frag.append(frag)
                                
                        if cpt_type_set == set(['r']):
                            concept_tag = OTHER # all aligned to relation
                            
                        elif len(cpt_labels) == 1 or (len(cpt_labels) == 2 and unique_frag[1][-1] == 'r'):
                            concept_tag = get_concept_tag(curr_amr, unique_frag[0])
                        else:
                            logger.writeln('Multiple concepts aligned: %s' % cpt_labels)
                            concept_tag = get_multi_concept_tag(curr_amr, unique_frag, tok)
                            logger.writeln('=> Subgraph concept tag: %s' % concept_tag)
                            
                candidate_tagset[concept_tag]+=1
                inst.concept_tags.append(concept_tag)
        label_wf.write(' '.join(inst.concept_tags)+'\n')
        logger.writeln(u' '.join(u'|'.join((w['form'],t)) for w,t in zip(inst.tokens[1:],inst.concept_tags)))
        logger.writeln(u' ')
    label_wf.close()
    #candidate_tagset = sorted([(u,v) for u,v in candidate_tagset.items()], key=lambda x:x[1], reverse=True)
    return candidate_tagset

def serialize_graph(amr, frags, tok, unlex=False, fuzzy_match_len=4):

    def fuzzy_match(str1, str2):
        return str1[:fuzzy_match_len] == str2[:fuzzy_match_len]
        
    i = 0
    stack = []
    edge_stack = []
    seq = []
    base_lvl = len(frags[0].split('.')) - 1
    lex = tok['lemma']
    matched_lex_index = None
    
    if len(frags) > 1 and frags[1][-1] == 'r': # no root
        i += 1
        frags = [None] + frags
        seq.append(PLACEHOLDER)
        
    while i < len(frags):
        frg = frags[i]
        frag_type, incoming, path = amr.get_concept_relation(frg)
        #assert frag_type == 'c'
        var = path[-1]
        clabel = amr.node_to_concepts.get(var,var)
        clabel, align = clabel.split('~')
        if fuzzy_match(clabel, lex):
            matched_lex_index = i
        
        curr_lvl = len(frg.split('.')) - base_lvl
        while curr_lvl <= len(stack):
            stack.pop()
            seq.append(')'+edge_stack.pop())
            
        if i == 0:
            stack.append(frg)
            seq.append(clabel)
            i+=1
        else:
            stack.append(frg)
            i += 1
            if i >= len(frags):
                logger.writeln('WARNING: separate subgraph detected! %s %s' % (seq, frags))
                break
            frag_type, incoming, path = amr.get_concept_relation(frags[i])
            edge = path[-1]
            edge = edge.split('~')[0]
            edge_stack.append(edge)
            seq.append(edge+'(')
            seq.append(clabel)

            i+=1
            
    seq.extend(')'+e for e in edge_stack[::-1])
    if unlex:
        if matched_lex_index == 0:
            seq[0] = PLACEHOLDER
        else:
            length = len(seq)
            for j in range(1,length):
                if not seq[j].startswith(')') and not seq[j].endswith('('): # edge
                    seq[j] = PLACEHOLDER
            

    return seq

    
def get_multi_concept_tag(curr_amr, frag, tok):
    '''
    get concept tag for multiple concept/relation (subgraph)
    '''
    assert len(frag) > 1
    sub_frag = []
    frag = sorted(frag, key=lambda x:x if x[-1] != 'r' else x[:-1])
    for i,f in enumerate(frag): # group
        if i > 0:
            curr_root = sub_frag[-1][0]
            if f[:len(curr_root)] == curr_root and (len(f) == len(frag[i-1])+2 or len(f) < len(frag[i-1])):
                sub_frag[-1].append(f)
            else:
                sub_frag.append([f])
        else:
            sub_frag.append([f])
    #logger.writeln(sub_frag)
    if len(sub_frag[0]) == 1:
        return get_concept_tag(curr_amr, sub_frag[0][0])
        
    graphseq = serialize_graph(curr_amr, sub_frag[0], tok, unlex=True) # always get the first
    
    subgraph_str = '_'.join(graphseq)

    return subgraph_str
    
    # subgraph_str_list = []
    # for sfrag in sub_frag:
    #     graphseq = serialize_graph(curr_amr, sfrag)
    #     subgraph_str = ' '.join(graphseq)
    #     subgraph_str_list.append(subgraph_str)

    # return '+'.join(subgraph_str_list)
    
def get_concept_tag(curr_amr, frag):
    '''
    get concept tag for single concept/relation
    '''
    def get_bio(curr_amr, parent, child):
        map_func = lambda y: curr_amr.node_to_concepts.get(y,y).split('~')
        stridx_func = lambda x: int(map_func(x)[1].split('.')[1]) if len(map_func(x)) > 1 else ''
        stridxs = sorted([v[0] for e,v in curr_amr[parent].items()], key=lambda x: stridx_func(x))
        if stridxs.index(child) == 0:
            return 'B'
        else:
            return 'I'
    def make_compiled_regex(rules):
        regexstr =  '|'.join('(?P<%s>%s)' % (name, rule) for name, rule in rules)
        return re.compile(regexstr)
    def isRole(curr_amr, parent, index):
        if parent is None:
            return False
        pcpt = curr_amr.node_to_concepts.get(parent,parent)
        incoming_edge = curr_amr[parent].items()[index][0]
        return re.match('^have-[^\s()-]+-role-\d+', pcpt) and incoming_edge == 'ARG2' 
        
    node_to_tok_lex = "(~e\.\d+(,\d+)*)?"
    cpt_lex_rules = [
        ('HAVEROLE', '^have-[^\s()-]+-role-\d+$'),
        ('PRED','^[^\s()]+-\d+$'),
        ('NEGATION', '^-$'),
        ('AMRUNK', '^amr-unknown$'),
        ('NONPRED','^[^\s:()]+$')
    ] # predicate
    cpt_re = make_compiled_regex(cpt_lex_rules)
    
    frag_type, incoming, path = curr_amr.get_concept_relation(frag)
    concept_tag = OTHER
    if frag_type == 'c': # concept
        parent, index = incoming
        var = path[-1]
        cpt_label = curr_amr.node_to_concepts.get(var, var)

        # const
        if isinstance(cpt_label, Quantity): # numerical related entity
            #bio_prefix = get_bio(curr_amr, parent, var)
            #concept_tag = [bio_prefix] + extract_quantity(curr_amr, path)
            concept_tag = extract_quantity(curr_amr, path) # ignore BI for now                                
        elif isinstance(cpt_label, StrLiteral): # named entity
            concept_tag = extract_name_entity(curr_amr, path)
            
        else: # non const
            cpt_label = cpt_label.split('~')[0].strip()
            
            match = cpt_re.match(cpt_label)
            if match is None:
                import pdb
                pdb.set_trace()
                print(cpt_label)
            cpt_type = match.lastgroup
            if cpt_type == 'PRED':
                verb, sense = cpt_label.rsplit('-',1)
                concept_tag = PRED % sense
            elif cpt_type == 'HAVEROLE': # have-*-role shouldn't be aligned
                concept_tag = OTHER
            elif cpt_type == 'NEGATION':
                concept_tag = NEGATION
            elif cpt_type == 'AMRUNK':
                concept_tag = AMRUNK
            elif cpt_type == 'NONPRED':
                concept_tag = NONPRED
                if isRole(curr_amr, parent, index): # parent is have-org-role
                    concept_tag = ROLE
            else:
                raise Exception('No concept type matched cpt:%s type:%s' % (cpt_label, cpt_type))

    else: # relation
        #print(var)
        parent, index = incoming
        rel = curr_amr[parent].items()[index][0].split('~')[0]
        #concept_tag.append(rel)
        
        # we also don't do relation for this stage
        concept_tag = OTHER

    return concept_tag

    
    
def test():
    instances = preprocess(FLAGS.input_amr)
    pprint(instances[1].toJSON())
  
if __name__ == '__main__':
    gflags.DEFINE_string('input_amr','./data/dev/dev.aligned.txt','input aligned/tokenized amr')
    argv=FLAGS(sys.argv)
    #test()
    basefname = FLAGS.input_amr
    output_label = basefname + '.label'
    candidate_tagset = categorize_concept(FLAGS.input_amr, output_label)
    pprint(sorted([(u,v) for u,v in candidate_tagset.items()], key=lambda x:x[1], reverse=True))
    tagset_file = basefname + '.tagset.pkl'
    with open(tagset_file, 'w') as tf:
        pkl.dump(candidate_tagset, tf)
  
    