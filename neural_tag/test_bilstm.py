# coding: utf-8
from __future__ import print_function

import sys
from data_prep import *
import numpy as np
import theano

from keras.models import Sequential
from keras.layers import Embedding, LSTM
from keras.layers import Bidirectional
from keras.layers import Dense, TimeDistributed
from keras.preprocessing import sequence
from bilstm import transform
from evaluator import compute_cm

import gflags
FLAGS=gflags.FLAGS
argv=FLAGS(sys.argv)


x_codebook = create_codebook('../data/npchunk/np_chunking_wsj.tok', '../data/npchunk/tok.codebook', threshold=3, plhd=UNK)
y_codebook = create_codebook('../data/npchunk/np_chunking_wsj.label', '../data/npchunk/bio.codebook', threshold=0, plhd=DICT)

print(x_codebook[0][:10])
print(y_codebook[0][:10])

token_to_ids('../data/npchunk/np_chunking_wsj.tok', x_codebook, '../data/npchunk/np_chunking_wsj.tok.ids', plhd=UNK)
token_to_ids('../data/npchunk/np_chunking_wsj.label', y_codebook, '../data/npchunk/np_chunking_wsj.label.ids', plhd=DICT)


x_ids_file='../data/npchunk/np_chunking_wsj.tok.ids'
y_ids_file='../data/npchunk/np_chunking_wsj.label.ids'

x_ids = [[int(i)+1 for i in line.split()] for line in open(x_ids_file)] # plus one for masking
y_ids = [[int(j)+1 for j in line.split()] for line in open(y_ids_file)]

#print('padding ...')
X = sequence.pad_sequences(x_ids, padding='post',truncating='post', maxlen=200)
y = sequence.pad_sequences(y_ids, padding='post',truncating='post', maxlen=200)


print('transforming y ...')
y = transform(y,len(y_codebook[0]))

train_X = X[:8900]
dev_X = X[8900:]

train_y = y[:8900]
dev_y = y_ids[8900:] # for testing, don't transform and pad


model=Sequential()

model.add(Embedding(len(x_codebook[0])+1, 128, input_length=200, mask_zero=True))
model.add(Bidirectional(LSTM(128, return_sequences=True)))
model.add(TimeDistributed(Dense(len(y_codebook[0])+1, activation='softmax')))

model.compile(
    loss='categorical_crossentropy',
    optimizer='rmsprop',
    metrics=['accuracy'],
)

def cal_acc(pred_seqs, gold_seqs):
    total=0.
    correct=0.
    for pseq, gseq in zip(pred_seqs,gold_seqs):
        for i,j in zip(pseq,gseq):
            total+=1.
            if i == j:
                correct+=1.

    return correct / total

print('fitting model ...')
np_epoch = 5
for i in range(np_epoch):
    model.fit(train_X, train_y, batch_size=50, nb_epoch=1)
    predicted_sequences = model.predict_classes(dev_X, batch_size=50,verbose=1)
    acc=cal_acc(predicted_sequences, dev_y)
    print('    Val acc: %.2f' % acc)
    sys.stdout.flush()
test_result = compute_cm(model, y_codebook[0], y_codebook[1], dev_X, dev_y)
_,_,f1,accuracy = test_result.print_out()


    
#predicted_sequences = model.predict_classes(dev_X, batch_size=50, verbose=1)
#with open('../data/npchunk/np_chunking_wsj.dev.label.ids.pred','w') as wf:
#    for seq in predicted_sequences:
#        wf.write(' '.join(str(i) for i in seq)+'\n')


                
                
