'''
prepare the data for the bilstm tagger
'''

from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
import os
import sys
import pickle as pkl
from collections import defaultdict

import gflags
FLAGS = gflags.FLAGS
gflags.DEFINE_integer('theta', 30, 'threshold to filter out DICT type')

UNK = '_unk_'
DICT = '<DICT>'


def create_codebook(seq_file, codebook_file, threshold=0, plhd=None, return_char=False):
    if not os.path.exists(codebook_file):
        print('creating codebook %s' % codebook_file, file=sys.stderr)
        counter = defaultdict(int)
        with open(seq_file, 'r') as f, open(codebook_file, 'w') as cf:
            for line in f:
                line = line.strip()
                for w in line.split():
                    if return_char:
                        for c in w:
                            counter[c] += 1
                    else:
                        counter[w] += 1
            sorted_items = sorted(
                counter.items(), key=lambda x: x[1], reverse=True)
            if threshold > 0:
                index2word = [plhd] + \
                    [k for k, v in sorted_items if v > threshold]
            else:
                index2word = [k for k, v in sorted_items]
            cf.write('\n'.join(index2word))
            word2index = dict([(w, i) for i, w in enumerate(index2word)])
            return (index2word, word2index)
    else:
        return load_codebook(codebook_file)


# def create_y_codebook(seq_file, codebook_file, theta):
#     if not os.path.exists(codebook_file):
#         counter = defaultdict(int)
#         with open(seq_file,'r') as f, open(codebook_file, 'w') as cf:
#             for line in f:
#                 line = line.strip()
#                 for l in line.split():
#                     counter[l] += 1

#             index2label = [DICT]+[k for k,v in counter.items() if v > theta]
#             cf.write('\n'.join(index2label))
#             label2index = dict([(w,i) for i,w in enumerate(index2label)])
#             return (index2label,label2index)
#     else:
#         return load_codebook(codebook_file)


def load_codebook(codebook_file):
    print('loading codebook %s' % codebook_file)
    with open(codebook_file, 'r') as f:
        index2entry = [w.strip() for w in f]
        entry2index = dict([(w, i) for i, w in enumerate(index2entry)])
    return (index2entry, entry2index)


def token_to_ids(seq_file, codebook, ids_file, plhd=None, return_char=False):
    if not os.path.exists(ids_file):
        print('creating ids file %s' % ids_file, file=sys.stderr)
        index2entry, entry2index = codebook
        with open(seq_file, 'r') as f, open(ids_file, 'w') as outf:
            for line in f:
                line = line.strip()
                if return_char:
                    defaultid = entry2index.get('_')
                    ids_line = ' '.join(
                        (','.join(str(entry2index.get(c, defaultid)) for c in w) for w in line.split()))
                else:
                    defaultid = entry2index.get(plhd, None)
                    ids_line = ' '.join(
                        (str(entry2index.get(w, defaultid)) for w in line.split()))
                outf.write(ids_line + '\n')


def sentence_to_ids(sentence, codebook, plhd=None, max_length=-1):
    index2entry, entry2index = codebook
    defaultid = entry2index.get(plhd, None)
    # plus one for mask zero
    sent_ids = [entry2index.get(w, defaultid) + 1 for w in sentence.split()]
    return sent_ids


def prep_data(train_dir, dev_dir, test_dir):
    '''
    prep data with only tok feature and label
    '''
    training_x_file = os.path.join(
        train_dir, 'training.aligned.txt.sent.tok')  # tokenized sentences
    training_y_file = os.path.join(train_dir, 'training.aligned.txt.label')
    tagset_file = os.path.join(train_dir, 'training.aligned.txt.tagset.pkl')

    x_codebook_file = os.path.join(train_dir, 'word.codebook')
    y_codebook_file = os.path.join(train_dir, 'label.codebook')
    x_codebook = create_codebook(
        training_x_file, x_codebook_file, threshold=3, plhd=UNK)
    y_codebook = create_codebook(
        training_y_file, y_codebook_file, threshold=FLAGS.theta, plhd=DICT)

    training_x_ids_file = os.path.join(train_dir, 'training.x.ids')
    training_y_ids_file = os.path.join(train_dir, 'training.y.ids')
    token_to_ids(training_x_file, x_codebook, training_x_ids_file, plhd=UNK)
    token_to_ids(training_y_file, y_codebook, training_y_ids_file, plhd=DICT)

    dev_x_file = os.path.join(
        dev_dir, 'dev.aligned.txt.sent.tok')  # tokenized sentences
    dev_y_file = os.path.join(dev_dir, 'dev.aligned.txt.label')
    dev_x_ids_file = os.path.join(dev_dir, 'dev.x.ids')
    dev_y_ids_file = os.path.join(dev_dir, 'dev.y.ids')
    token_to_ids(dev_x_file, x_codebook, dev_x_ids_file, plhd=UNK)
    token_to_ids(dev_y_file, y_codebook, dev_y_ids_file, plhd=DICT)

    return (x_codebook_file, y_codebook_file,
            training_x_ids_file, training_y_ids_file,
            dev_x_ids_file, dev_y_ids_file)


def create_codebook_util(data_dir, data_type):
    data_word_file = os.path.join(
        data_dir, '%s.aligned.txt.sent.tok' % data_type)
    data_lemma_file = os.path.join(
        data_dir, '%s.aligned.txt.sent.lemma' % data_type)
    data_pos_file = os.path.join(
        data_dir, '%s.aligned.txt.sent.pos' % data_type)
    data_ner_file = os.path.join(
        data_dir, '%s.aligned.txt.sent.ner' % data_type)
    data_pfx_file = os.path.join(
        data_dir, '%s.aligned.txt.sent.pfx' % data_type)
    data_sfx_file = os.path.join(
        data_dir, '%s.aligned.txt.sent.sfx' % data_type)

    word_codebook_file = os.path.join(data_dir, 'word.codebook')
    lemma_codebook_file = os.path.join(data_dir, 'lemma.codebook')
    pos_codebook_file = os.path.join(data_dir, 'pos.codebook')
    ner_codebook_file = os.path.join(data_dir, 'ner.codebook')
    pfx_codebook_file = os.path.join(data_dir, 'pfx.codebook')
    sfx_codebook_file = os.path.join(data_dir, 'sfx.codebook')
    char_codebook_file = os.path.join(data_dir, 'char.codebook')

    word_codebook = create_codebook(
        data_word_file, word_codebook_file, threshold=3, plhd=UNK)
    lemma_codebook = create_codebook(
        data_lemma_file, lemma_codebook_file, threshold=3, plhd=UNK)
    pos_codebook = create_codebook(data_pos_file, pos_codebook_file)
    ner_codebook = create_codebook(data_ner_file, ner_codebook_file)
    pfx_codebook = create_codebook(
        data_pfx_file, pfx_codebook_file, threshold=3, plhd=UNK)
    sfx_codebook = create_codebook(
        data_sfx_file, sfx_codebook_file, threshold=3, plhd=UNK)
    char_codebook = create_codebook(
        data_word_file, char_codebook_file, return_char=True)

    data_y_file = os.path.join(data_dir, '%s.aligned.txt.label' % data_type)
    y_codebook_file = os.path.join(data_dir, 'label.codebook')
    y_codebook = create_codebook(
        data_y_file, y_codebook_file, threshold=FLAGS.theta, plhd=DICT)

    return (word_codebook, lemma_codebook, pos_codebook, ner_codebook, pfx_codebook, sfx_codebook, char_codebook), y_codebook


def prep_data_multi_util(data_dir, data_type, x_codebooks, y_codebook):

    data_word_file = os.path.join(
        data_dir, '%s.aligned.txt.sent.tok' % data_type)
    data_lemma_file = os.path.join(
        data_dir, '%s.aligned.txt.sent.lemma' % data_type)
    data_pos_file = os.path.join(
        data_dir, '%s.aligned.txt.sent.pos' % data_type)
    data_ner_file = os.path.join(
        data_dir, '%s.aligned.txt.sent.ner' % data_type)
    data_pfx_file = os.path.join(
        data_dir, '%s.aligned.txt.sent.pfx' % data_type)
    data_sfx_file = os.path.join(
        data_dir, '%s.aligned.txt.sent.sfx' % data_type)

    word_codebook, lemma_codebook, pos_codebook, ner_codebook, pfx_codebook, sfx_codebook, char_codebook = x_codebooks

    data_word_ids_file = os.path.join(data_dir, '%s.word.ids' % data_type)
    data_lemma_ids_file = os.path.join(data_dir, '%s.lemma.ids' % data_type)
    data_pos_ids_file = os.path.join(data_dir, '%s.pos.ids' % data_type)
    data_ner_ids_file = os.path.join(data_dir, '%s.ner.ids' % data_type)
    data_pfx_ids_file = os.path.join(data_dir, '%s.pfx.ids' % data_type)
    data_sfx_ids_file = os.path.join(data_dir, '%s.sfx.ids' % data_type)
    data_char_ids_file = os.path.join(data_dir, '%s.char.ids' % data_type)

    token_to_ids(data_word_file, word_codebook, data_word_ids_file, plhd=UNK)
    token_to_ids(data_lemma_file, lemma_codebook,
                 data_lemma_ids_file, plhd=UNK)
    token_to_ids(data_pos_file, pos_codebook, data_pos_ids_file)
    token_to_ids(data_ner_file, ner_codebook, data_ner_ids_file)
    token_to_ids(data_pfx_file, pfx_codebook, data_pfx_ids_file, plhd=UNK)
    token_to_ids(data_sfx_file, sfx_codebook, data_sfx_ids_file, plhd=UNK)
    token_to_ids(data_word_file, char_codebook,
                 data_char_ids_file, return_char=True)

    # labels
    data_y_file = os.path.join(data_dir, '%s.aligned.txt.label' % data_type)
    data_y_ids_file = os.path.join(data_dir, '%s.y.ids' % data_type)
    token_to_ids(data_y_file, y_codebook, data_y_ids_file, plhd=DICT)

    return data_word_ids_file, data_lemma_ids_file, data_pos_ids_file, data_ner_ids_file, data_pfx_ids_file, data_sfx_ids_file, data_char_ids_file, data_y_ids_file


def prep_data_multi_feat(train_dir, dev_dir, test_dir):
    '''
    prep data with label and multiple features
    '''
    x_codebooks, y_codebook = create_codebook_util(train_dir, 'training')
    training_ids_files = prep_data_multi_util(
        train_dir, 'training', x_codebooks, y_codebook)
    dev_ids_files = prep_data_multi_util(
        dev_dir, 'dev', x_codebooks, y_codebook)
    #training_ids_files = training_word_ids_file,training_lemma_ids_file,training_pos_ids_file,training_ner_ids_file,training_pfx_ids_file,training_sfx_ids_file, training_y_ids_file
    #dev_ids_files = dev_word_ids_file,dev_lemma_ids_file,dev_pos_ids_file,dev_ner_ids_file,dev_pfx_ids_file,dev_sfx_ids_file, dev_y_ids_file

    return training_ids_files, dev_ids_files


if __name__ == '__main__':
    gflags.DEFINE_string('train_dir', '../data/training', 'training directory')
    gflags.DEFINE_string('dev_dir', '../data/dev', 'dev directory')
    gflags.DEFINE_string('test_dir', '../data/test', 'test directory')

    argv = FLAGS(sys.argv)

    # prep_data(FLAGS.train_dir,FLAGS.dev_dir,FLAGS.test_dir)
    # prep_data_multi_feat(FLAGS.train_dir,FLAGS.dev_dir,FLAGS.test_dir)
