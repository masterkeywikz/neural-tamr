'''
bilstm tagger
'''

from __future__ import print_function
from __future__ import division
from __future__ import absolute_import

import sys
import os
import time
import numpy as np
import theano

from keras.models import Sequential, load_model
from keras.layers import Embedding, LSTM, Merge
from keras.layers import Bidirectional
from keras.layers import Dense, TimeDistributed, Dropout, Permute, Reshape
from keras.layers.convolutional import Convolution1D, MaxPooling1D, Convolution2D, MaxPooling2D
from keras.preprocessing import sequence
from keras.metrics import sparse_categorical_accuracy

from data_prep import prep_data, load_codebook, prep_data_multi_feat, sentence_to_ids
from data_prep import UNK, DICT
from evaluator import compute_cm

import gflags
FLAGS = gflags.FLAGS
gflags.DEFINE_integer('input_word_vocab_size', 7284,
                      'input dim for word embedding layer')
gflags.DEFINE_integer('input_lemma_vocab_size', 5884,
                      'input dim for lemma embedding layer')
gflags.DEFINE_integer('input_ner_vocab_size', 13,
                      'input dim for ner embedding layer')
gflags.DEFINE_integer('input_pfx_vocab_size', 846,
                      'input dim for prefix layer')
gflags.DEFINE_integer('input_sfx_vocab_size', 764,
                      'input dim for suffix embedding layer')
gflags.DEFINE_integer('input_char_vocab_size', 103,
                      'input dim for char embedding layer')
gflags.DEFINE_integer('output_label_vocab_size', 116, 'output tag size')
gflags.DEFINE_integer('max_length', 200, 'max length of input sentence')
gflags.DEFINE_integer('max_charlen', 20, 'max length of input word')
gflags.DEFINE_integer('word_embedding_size', 128, 'word embedding size')
gflags.DEFINE_integer('lemma_embedding_size', 128, 'lemma embedding size')
gflags.DEFINE_integer('ner_embedding_size', 8, 'ner embedding size')
gflags.DEFINE_integer('pfx_embedding_size', 32, 'prefix embedding size')
gflags.DEFINE_integer('sfx_embedding_size', 32, 'suffix embedding size')
gflags.DEFINE_integer('char_embedding_size', 50, 'character embedding size')
gflags.DEFINE_integer('hidden_size', 128, 'hidden layer size')
gflags.DEFINE_integer('batch_size', 50, 'batch size')
gflags.DEFINE_integer('nb_epoch', 10, 'number of epoch')
gflags.DEFINE_integer('nb_filters', 10, 'number of filters for cnn')


def transform(y, y_vocab_size):
    ''' Transforms every training instance in y from
    index representation to a list of one-hot representations.
    E.g. with vocabulary size 3, [0, 1, 2] is transformed into
    [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
    '''

    transformed_y = []
    for i in range(len(y)):
        if i % 1000 == 0:
            print('%d ... ' % i, end='')
            sys.stdout.flush()
        transformed_y.append([])
        for digit in y[i]:
            tmp = [0 for j in range(y_vocab_size + 1)]
            tmp[digit] = 1
            transformed_y[-1].append(tmp)

    return np.array(transformed_y, dtype='int32')


def load_data(train_dir, dev_dir, test_dir):
    _, y_codebook_file, training_x_ids_file, training_y_ids_file, dev_x_ids_file, dev_y_ids_file = prep_data(
        train_dir, dev_dir, test_dir)

    train_x, train_y = read_data(training_x_ids_file, training_y_ids_file)
    dev_x, dev_y = read_data(dev_x_ids_file, dev_y_ids_file, train=False)

    return y_codebook_file, train_x, train_y, dev_x, dev_y


def read_data(x_ids_file, y_ids_file, train=True):
    x_ids = [[int(i) + 1 for i in line.split()]
             for line in open(x_ids_file)]  # plus one for masking
    y_ids = [[int(j) + 1 for j in line.split()] for line in open(y_ids_file)]

    print('padding ...')
    x = sequence.pad_sequences(
        x_ids, padding='post', truncating='post', maxlen=FLAGS.max_length)

    if train:
        y = sequence.pad_sequences(
            y_ids, padding='post', truncating='post', maxlen=FLAGS.max_length)

        print('transforming y ...')
        y = transform(y, FLAGS.output_label_vocab_size)
    else:
        y = y_ids

    return x, y


def load_data_multi(train_dir, dev_dir, test_dir):
    training_ids_file, dev_ids_file = prep_data_multi_feat(
        train_dir, dev_dir, test_dir)
    training_word_ids_file = training_ids_file[0]
    training_lemma_ids_file = training_ids_file[1]
    training_pos_ids_file = training_ids_file[2]
    training_ner_ids_file = training_ids_file[3]
    training_pfx_ids_file = training_ids_file[4]
    training_sfx_ids_file = training_ids_file[5]
    training_char_ids_file = training_ids_file[6]

    train_x_word = read_data_multi_util(training_word_ids_file, padding=True)
    train_x_lemma = read_data_multi_util(training_lemma_ids_file, padding=True)
    train_x_pos = read_data_multi_util(training_pos_ids_file, padding=True)
    train_x_ner = read_data_multi_util(training_ner_ids_file, padding=True)
    train_x_pfx = read_data_multi_util(training_pfx_ids_file, padding=True)
    train_x_sfx = read_data_multi_util(training_sfx_ids_file, padding=True)
    train_x_char = read_data_multi_util(
        training_char_ids_file, return_char=True)

    training_y_ids_file = training_ids_file[-1]
    train_y = read_data_multi_util(
        training_y_ids_file, padding=True, trans=True)

    dev_word_ids_file = dev_ids_file[0]
    dev_lemma_ids_file = dev_ids_file[1]
    dev_pos_ids_file = dev_ids_file[2]
    dev_ner_ids_file = dev_ids_file[3]
    dev_pfx_ids_file = dev_ids_file[4]
    dev_sfx_ids_file = dev_ids_file[5]
    dev_char_ids_file = dev_ids_file[6]

    dev_x_word = read_data_multi_util(dev_word_ids_file, padding=True)
    dev_x_lemma = read_data_multi_util(dev_lemma_ids_file, padding=True)
    dev_x_pos = read_data_multi_util(dev_pos_ids_file, padding=True)
    dev_x_ner = read_data_multi_util(dev_ner_ids_file, padding=True)
    dev_x_pfx = read_data_multi_util(dev_pfx_ids_file, padding=True)
    dev_x_sfx = read_data_multi_util(dev_sfx_ids_file, padding=True)
    dev_x_char = read_data_multi_util(dev_char_ids_file, return_char=True)

    dev_y_ids_file = dev_ids_file[-1]
    dev_y = read_data_multi_util(dev_y_ids_file)

    return train_x_word, train_x_lemma, train_x_pos, train_x_ner, train_x_pfx, train_x_sfx, train_x_char, train_y, dev_x_word, dev_x_lemma, dev_x_pos, dev_x_ner, dev_x_pfx, dev_x_sfx, dev_x_char, dev_y


def read_data_multi_util(ids_file, padding=False, trans=False, return_char=False):
    x = None
    if return_char:
        x = [[[int(line.split()[i].split(',')[j]) + 1 if j < len(line.split()[i]) else 0 for j in range(FLAGS.max_charlen)] if i < len(line.split())
              else [0] * FLAGS.max_charlen for i in range(FLAGS.max_length)] for line in open(ids_file)]  # plus one for masking
    else:
        x = [[int(i) + 1 for i in line.split()]
             for line in open(ids_file)]  # plus one for masking

    if padding and not return_char:
        print('padding ...')
        x = sequence.pad_sequences(
            x, padding='post', truncating='post', maxlen=FLAGS.max_length)

    if trans:
        print('transforming y ...')
        x = transform(x, FLAGS.output_label_vocab_size)
    return np.array(x)


def cal_acc(pred_seqs, gold_seqs):
    total = 0.
    correct = 0.
    for pseq, gseq in zip(pred_seqs, gold_seqs):
        for i, j in zip(pseq, gseq):
            total += 1.
            if i == j:
                correct += 1.

    return correct / total


def train():
    y_codebook_file, train_x, train_y, dev_x, dev_y = load_data(
        FLAGS.train_dir, FLAGS.dev_dir, FLAGS.test_dir)
    y_codebook = load_codebook(y_codebook_file)
    model = Sequential()

    model.add(Embedding(FLAGS.input_word_vocab_size + 1,
                        FLAGS.word_embedding_size, input_length=FLAGS.max_length, mask_zero=True))
    model.add(Bidirectional(LSTM(FLAGS.hidden_size, return_sequences=True)))
    model.add(TimeDistributed(
        Dense(FLAGS.output_label_vocab_size + 1, activation='softmax')))

    model.compile(
        loss='categorical_crossentropy',
        optimizer='rmsprop',
        metrics=['accuracy'],
    )

    print('fitting model ...')

    #model.fit(train_x, train_y, batch_size=FLAGS.batch_size, nb_epoch=FLAGS.nb_epoch, validation_data=[dev_x, dev_y])
    nb_epoch = FLAGS.nb_epoch
    for i in range(nb_epoch):
        print('Epoch %d/%d ' % (i, nb_epoch), end='')
        model.fit(train_x, train_y, batch_size=FLAGS.batch_size, nb_epoch=1)
        predicted_sequences = model.predict_classes(
            dev_x, batch_size=FLAGS.batch_size, verbose=1)
        acc = cal_acc(predicted_sequences, dev_y)
        print('Validation Accuracy: %.4f' % acc)
        sys.stdout.flush()

    # return model.predict_classes(dev_x, batch_size=FLAGS.batch_size, verbose=1)


def train_multi_feat():
    train_x_word, train_x_lemma, _, train_x_ner, train_x_pfx, train_x_sfx, _, train_y, dev_x_word, dev_x_lemma, _, dev_x_ner, dev_x_pfx, dev_x_sfx, _, dev_y = load_data_multi(
        FLAGS.train_dir, FLAGS.dev_dir, FLAGS.test_dir)

    y_codebook_path = os.path.join(FLAGS.train_dir, 'label.codebook')
    y_codebook = load_codebook(y_codebook_path)

    train_x_set = [train_x_word, train_x_ner, train_x_pfx, train_x_sfx]
    dev_x_set = [dev_x_word, dev_x_ner, dev_x_pfx, dev_x_sfx]

    # word+ner
    word_emb = Sequential()
    word_emb.add(Embedding(FLAGS.input_word_vocab_size + 1,
                           FLAGS.word_embedding_size, input_length=FLAGS.max_length, mask_zero=True))

    ner_emb = Sequential()
    ner_emb.add(Embedding(FLAGS.input_ner_vocab_size + 1,
                          FLAGS.ner_embedding_size, input_length=FLAGS.max_length, mask_zero=True))

    pfx_emb = Sequential()
    pfx_emb.add(Embedding(FLAGS.input_pfx_vocab_size + 1,
                          FLAGS.pfx_embedding_size, input_length=FLAGS.max_length, mask_zero=True))

    sfx_emb = Sequential()
    sfx_emb.add(Embedding(FLAGS.input_sfx_vocab_size + 1,
                          FLAGS.sfx_embedding_size, input_length=FLAGS.max_length, mask_zero=True))

    merged = Merge([word_emb, ner_emb, pfx_emb, sfx_emb], mode='concat')

    model = Sequential()
    model.add(merged)

    '''
    # lemma only
    model=Sequential()
    model.add(Embedding(FLAGS.input_lemma_vocab_size+1, FLAGS.lemma_embedding_size, input_length=FLAGS.max_length, mask_zero=True))
    '''

    model.add(Bidirectional(LSTM(FLAGS.hidden_size, return_sequences=True)))
    model.add(TimeDistributed(
        Dense(FLAGS.output_label_vocab_size + 1, activation='softmax')))

    model.compile(
        loss='categorical_crossentropy',
        optimizer='rmsprop',
        metrics=['accuracy'],
    )

    print('fitting model ...')

    #model.fit(train_x, train_y, batch_size=FLAGS.batch_size, nb_epoch=FLAGS.nb_epoch, validation_data=[dev_x, dev_y])
    best = 0.
    patience = 20
    wait = 0
    min_delta = 1e-3
    start_time = time.time()

    nb_epoch = 1
    best_epoch = 1
    #nb_epoch = FLAGS.nb_epoch
    # for i in range(nb_epoch):
    # while nb_epoch < 21:
    while True:
        print('Epoch %d ' % (nb_epoch), end='')
        model.fit(train_x_set, train_y,
                  batch_size=FLAGS.batch_size, nb_epoch=1)
        predicted_sequences = model.predict_classes(
            dev_x_set, batch_size=FLAGS.batch_size, verbose=1)
        acc = cal_acc(predicted_sequences, dev_y)
        print('Validation Accuracy: %.4f' % acc)
        if acc - best > min_delta:
            model.save(FLAGS.save_model)
            best = acc
            best_epoch = nb_epoch
            wait = 0
        else:
            if wait >= patience:
                print('Stopped at epoch %d, best accuracy achieved %.4f' %
                      (nb_epoch, best))
                break
            wait += 1
        sys.stdout.flush()
        nb_epoch += 1

    end_time = time.time()
    print('Done training. Best epoch %d, time used %.2f min' %
          (best_epoch, (end_time - start_time) / 60.))
    best_model = load_model(FLAGS.save_model)
    test_result = compute_cm(
        best_model, y_codebook[0], y_codebook[1], dev_x_set, dev_y)
    _, _, f1, accuracy = test_result.print_out()


def train_multi_feat_cnn():
    train_x_word, _, _, train_x_ner, _, _, train_x_char, train_y, dev_x_word, _, _, dev_x_ner, _, _, dev_x_char, dev_y = load_data_multi(
        FLAGS.train_dir, FLAGS.dev_dir, FLAGS.test_dir)

    y_codebook_path = os.path.join(FLAGS.train_dir, 'label.codebook')
    y_codebook = load_codebook(y_codebook_path)

    train_x_char = train_x_char.reshape(
        (train_x_char.shape[0], train_x_char.shape[1] * train_x_char.shape[2]))
    dev_x_char = dev_x_char.reshape(
        (dev_x_char.shape[0], dev_x_char.shape[1] * dev_x_char.shape[2]))

    train_x_set = [train_x_word, train_x_ner, train_x_char]
    dev_x_set = [dev_x_word, dev_x_ner, dev_x_char]

    # word+ner
    word_emb = Sequential()
    word_emb.add(Embedding(FLAGS.input_word_vocab_size + 1,
                           FLAGS.word_embedding_size, input_length=FLAGS.max_length, mask_zero=True))

    ner_emb = Sequential()
    ner_emb.add(Embedding(FLAGS.input_ner_vocab_size + 1,
                          FLAGS.ner_embedding_size, input_length=FLAGS.max_length, mask_zero=True))

    print("Building sequential CNN model for char based word embeddings")
    model_cnn = Sequential()
    model_cnn.add(Embedding(FLAGS.input_char_vocab_size + 1, FLAGS.char_embedding_size,
                            input_length=FLAGS.max_length * FLAGS.max_charlen))
    model_cnn.add(
        Reshape((FLAGS.max_length, FLAGS.max_charlen, FLAGS.char_embedding_size)))
    model_cnn.add(Permute((3, 1, 2)))
    model_cnn.add(Convolution2D(FLAGS.nb_filters, 1, 2,
                                border_mode='same', dim_ordering='th'))
    model_cnn.add(Permute((2, 1, 3)))
    model_cnn.add(MaxPooling2D((2, 2), dim_ordering='th'))
    model_cnn.add(Reshape((FLAGS.max_length, 50)))

    merged = Merge([word_emb, ner_emb, model_cnn], mode='concat')

    model = Sequential()
    model.add(merged)
    model.add(Dropout(0.5))

    '''
    # lemma only
    model=Sequential()
    model.add(Embedding(FLAGS.input_lemma_vocab_size+1, FLAGS.lemma_embedding_size, input_length=FLAGS.max_length, mask_zero=True))
    '''

    model.add(Bidirectional(LSTM(FLAGS.hidden_size, return_sequences=True)))
    model.add(Dropout(0.5))
    model.add(TimeDistributed(
        Dense(FLAGS.output_label_vocab_size + 1, activation='softmax')))

    model.compile(
        loss='categorical_crossentropy',
        optimizer='rmsprop',
        metrics=['accuracy'],
    )

    print('fitting model ...')

    #model.fit(train_x, train_y, batch_size=FLAGS.batch_size, nb_epoch=FLAGS.nb_epoch, validation_data=[dev_x, dev_y])
    best = 0.
    patience = 20
    wait = 0
    min_delta = 1e-3
    start_time = time.time()

    nb_epoch = 1
    best_epoch = 1
    #nb_epoch = FLAGS.nb_epoch
    # for i in range(nb_epoch):
    # while nb_epoch < 21:
    while True:
        print('Epoch %d ' % (nb_epoch), end='')
        model.fit(train_x_set, train_y,
                  batch_size=FLAGS.batch_size, nb_epoch=1)
        predicted_sequences = model.predict_classes(
            dev_x_set, batch_size=FLAGS.batch_size, verbose=1)
        acc = cal_acc(predicted_sequences, dev_y)
        print('Validation Accuracy: %.4f' % acc)
        if acc - best > min_delta:
            model.save(FLAGS.save_model)
            best = acc
            best_epoch = nb_epoch
            wait = 0
        else:
            if wait >= patience:
                print('Stopped at epoch %d, best accuracy achieved %.4f' %
                      (nb_epoch, best))
                break
            wait += 1
        sys.stdout.flush()
        nb_epoch += 1

    end_time = time.time()
    print('Done training. Best epoch %d, time used %.2f min' %
          (best_epoch, (end_time - start_time) / 60.))
    best_model = load_model(FLAGS.save_model)
    test_result = compute_cm(
        best_model, y_codebook[0], y_codebook[1], dev_x_set, dev_y)
    _, _, f1, accuracy = test_result.print_out()


def test(evaluation=True):
    print('Loading model %s' % FLAGS.save_model, file=sys.stderr)
    model = load_model(FLAGS.save_model)

    y_codebook_path = os.path.join(FLAGS.train_dir, 'label.codebook')
    y_codebook = load_codebook(y_codebook_path)

    word_codebook_path = os.path.join(FLAGS.train_dir, 'word.codebook')
    word_codebook = load_codebook(word_codebook_path)
    lemma_codebook_path = os.path.join(FLAGS.train_dir, 'lemma.codebook')
    lemma_codebook = load_codebook(lemma_codebook_path)
    pos_codebook_path = os.path.join(FLAGS.train_dir, 'pos.codebook')
    pos_codebook = load_codebook(pos_codebook_path)
    ner_codebook_path = os.path.join(FLAGS.train_dir, 'ner.codebook')
    ner_codebook = load_codebook(ner_codebook_path)
    pfx_codebook_path = os.path.join(FLAGS.train_dir, 'pfx.codebook')
    pfx_codebook = load_codebook(pfx_codebook_path)
    sfx_codebook_path = os.path.join(FLAGS.train_dir, 'sfx.codebook')
    sfx_codebook = load_codebook(sfx_codebook_path)

    test_x_word_path = os.path.join(FLAGS.test_dir, FLAGS.test_fn + '.tok')
    test_x_lemma_path = os.path.join(FLAGS.test_dir, FLAGS.test_fn + '.lemma')
    test_x_pos_path = os.path.join(FLAGS.test_dir, FLAGS.test_fn + '.pos')
    test_x_ner_path = os.path.join(FLAGS.test_dir, FLAGS.test_fn + '.ner')
    test_x_pfx_path = os.path.join(FLAGS.test_dir, FLAGS.test_fn + '.pfx')
    test_x_sfx_path = os.path.join(FLAGS.test_dir, FLAGS.test_fn + '.sfx')

    test_x_word = []
    test_x_lemma = []
    test_x_pos = []
    test_x_ner = []
    test_x_pfx = []
    test_x_sfx = []

    with open(test_x_word_path) as wrdf, open(test_x_lemma_path) as lmf, open(test_x_pos_path) as posf, open(test_x_ner_path) as nerf, open(test_x_pfx_path) as pfxf, open(test_x_sfx_path) as sfxf:
        for w, l, p, n, px, sx in zip(wrdf, lmf, posf, nerf, pfxf, sfxf):
            tok_ids = sentence_to_ids(w, word_codebook, plhd=UNK)
            lemma_ids = sentence_to_ids(l, lemma_codebook, plhd=UNK)
            pos_ids = sentence_to_ids(p, pos_codebook, plhd="''")
            ner_ids = sentence_to_ids(n, ner_codebook)
            pfx_ids = sentence_to_ids(px, pfx_codebook, plhd=UNK)
            sfx_ids = sentence_to_ids(sx, sfx_codebook, plhd=UNK)

            test_x_word.append(tok_ids)
            test_x_lemma.append(lemma_ids)
            test_x_pos.append(pos_ids)
            test_x_ner.append(ner_ids)
            test_x_pfx.append(pfx_ids)
            test_x_sfx.append(sfx_ids)

    test_y = []
    test_y_path = os.path.join(FLAGS.test_dir, FLAGS.test_fn + '.label')
    if evaluation:
        with open(test_y_path) as yf:
            for y in yf:
                y_ids = sentence_to_ids(y, y_codebook, plhd=DICT)
                test_y.append(y_ids)

    test_x_word_padded = sequence.pad_sequences(
        test_x_word, padding='post', truncating='post', maxlen=FLAGS.max_length)
    test_x_lemma_padded = sequence.pad_sequences(
        test_x_lemma, padding='post', truncating='post', maxlen=FLAGS.max_length)
    test_x_pos_padded = sequence.pad_sequences(
        test_x_pos, padding='post', truncating='post', maxlen=FLAGS.max_length)
    test_x_ner_padded = sequence.pad_sequences(
        test_x_ner, padding='post', truncating='post', maxlen=FLAGS.max_length)
    test_x_pfx_padded = sequence.pad_sequences(
        test_x_pfx, padding='post', truncating='post', maxlen=FLAGS.max_length)
    test_x_sfx_padded = sequence.pad_sequences(
        test_x_sfx, padding='post', truncating='post', maxlen=FLAGS.max_length)

    #test_x_set = [test_x_word_padded, test_x_ner_padded, test_x_pfx_padded, test_x_sfx_padded]
    test_x_set = [test_x_word_padded, test_x_ner_padded]
    print('Testing %s' % FLAGS.test_dir)
    predicted_sequences = model.predict_classes(
        test_x_set, batch_size=FLAGS.batch_size, verbose=1)
    test_y_pred_path = test_y_path + '.pred'
    with open(test_y_pred_path, 'w') as prdf:
        for i in range(len(predicted_sequences)):
            length = len(test_x_word[i])
            index2label = y_codebook[0]
            prdf.write(
                ' '.join(index2label[py - 1] for py in predicted_sequences[i][:length]) + '\n')
    print('Done testing. Predicted file %s' % test_y_pred_path)
    if evaluation:
        test_result = compute_cm(
            model, y_codebook[0], y_codebook[1], test_x_set, test_y)
        _, _, f1, accuracy = test_result.print_out()


if __name__ == '__main__':
    gflags.DEFINE_string(
        'train_dir', '../data/zh/training', 'training directory')
    gflags.DEFINE_string('dev_dir', '../data/zh/dev', 'dev directory')
    gflags.DEFINE_string('test_dir', '../data/zh/test', 'test directory')
    gflags.DEFINE_string('test_fn', 'training.txt', 'test file name')
    #gflags.DEFINE_string('label_codebook', '../data/training/label.codebook','label codebook')
    gflags.DEFINE_string(
        'save_model', '../models/zh/model.word.h5', 'save model path')
    gflags.DEFINE_string('mode', 'train', 'mode to run')
    gflags.DEFINE_boolean('eval', False, 'whether to do evaluation')
    argv = FLAGS(sys.argv)

    if FLAGS.mode == 'train':
        train()
    elif FLAGS.mode == 'train-multi':
        train_multi_feat()
    elif FLAGS.mode == 'train-multi-cnn':
        train_multi_feat_cnn()
    elif FLAGS.mode == 'test':
        test(evaluation=FLAGS.eval)
    elif FLAGS.mode == 'cross':  # cross validation
        cross_validation()
    else:
        raise Exception('Unknown mode %s' % FLAGS.mode)
